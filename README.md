# Using Mask R-CNN to detect fire hydrant
 
```

    # Apply color splash to an image
    python3 custom.py splash --weights=/path/to/weights/file.h5 --image=<URL or path to file>


```



## Install pre-requisites
```
$ pip install -r requirements.txt
```

